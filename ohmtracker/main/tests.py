from django.test import TestCase

from main.models import *
from main.management.commands.incrementalupdate import Command

class MainTestCase(TestCase):
    def setUp(self):
        # instantiate objects
        chain = Chain(
                name='ethereum',
                total_supply_query='https://api.etherscan.io/api?module=stats&action=tokensupply&contractaddress={address}&apikey=V3EY284WGX8AM7DIYSEDS3HBAMUPZM611C',
                token_balance_query='https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress={token}&address={pool}&apikey=V3EY284WGX8AM7DIYSEDS3HBAMUPZM611C'
        )
        chain.save()
        ohm = Token(
                chain=chain,
                address='0x383518188c0c6d7730d91b2c03a03c837814a899',
                decimals=9,
                name='ohm'
        )
        ohm.save()
        olympus = DAO(
                name='olympus', 
                treasury='0x31F8Cc382c9898b273eff4e0b7626a6987C846E8', 
                token=ohm, 
                public=True
        )
        olympus.save()
        lusd = Token(
                chain=chain,
                address='0x5f98805A4E8be255a32880FDeC7F6728C6568bA0',
                name='LUSD'
        )
        lusd.save()
        ohm_lusd = PoolToken(
                chain=chain,
                address='0xfDf12D1F85b5082877A6E070524f50F6c84FAa6b',
                exchange_name='SushiSwap'
        )
        ohm_lusd.save()
        PoolMember(member=ohm, pool_token=ohm_lusd).save()
        PoolMember(member=lusd, pool_token=ohm_lusd).save()
        TreasuryBalance(dao=olympus, token=ohm_lusd).save()

        # execute incremental updates
        command = Command()
        command.updatePrice(0)
        command.updatePrice(1)
        command.updatePoolBalance(0)
        command.updatePoolBalance(1)
        command.updatePrice(2)
        command.updateTreasuryBalance(0)

    def testHomePage(self):
        response = self.client.get('')
        self.assertContains(response, 'olympus')

    def testDaoJson(self):
        response = self.client.get('/dao/json/olympus')
        self.assertContains(response, 'lusd')

    def testFullIncrementalUpdate(self):
        command = Command()
        command.handle()
