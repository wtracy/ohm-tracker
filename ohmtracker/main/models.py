from decimal import Decimal

from django.db import models
from django.urls import reverse

from model_utils.managers import InheritanceManager
import requests
from pycoingecko import CoinGeckoAPI


class Chain(models.Model):
    class Meta:
        pass

    name = models.CharField(max_length=100, primary_key=True)

    total_supply_query = models.CharField(max_length=200)
    token_balance_query = models.CharField(max_length=200)
    address_url = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name

    def getSupply(self, address):
        query = self.total_supply_query.format(address=address)
        r = requests.get(query)
        return r.json()['result']

    def getBalance(self, pool, token):
        query = self.token_balance_query.format(pool=pool, token=token)
        r = requests.get(query).json()
        return r['result']

    def getUrl(self, address):
        query = self.address_url.format(address=address)
        return query

class Token(models.Model):
    class Meta:
        pass

    objects = InheritanceManager()

    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    address = models.CharField(max_length=100, unique=True)
    decimals = models.IntegerField(default=18)

    tid = models.CharField(max_length=100, null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    price = models.DecimalField(max_digits=65, decimal_places=10, null=True, blank=True)
    supply = models.DecimalField(max_digits=65, decimal_places=10, null=True, blank=True)
    market_cap = models.DecimalField(max_digits=65, decimal_places=10, null=True, blank=True)
    price_last_updated = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        if self.name == None:
            return self.address
        return self.name

    def update(self):
        cg = CoinGeckoAPI()
        result = cg.get_coin_info_from_contract_address_by_id(self.chain.name, self.address, vs_currencies='usd')

        self.name = result["symbol"]
        self.tid = result["id"]
        self.price = (result['market_data']['current_price']['usd'])
        self.market_cap = result['market_data']['market_cap']['usd']
        self.supply = result['market_data']['total_supply']
        self.priceLastUpdated = result['last_updated']

        # sometimes Coingecko gives us a zero market cap for no reason
        if (self.supply == None):
            self.supply = 0
        if (self.market_cap <= 0):
            self.market_cap = self.price * self.supply

        self.save()

    def referenceUrl(self):
        if self.tid != None:
            return 'https://coingecko.com/en/coins/' + self.tid
        return self.chain.getUrl(self.address)

    def absentFromTreasuries(self):
        return self.treasuryEntries() == 0

    def treasuryEntries(self):
        return TreasuryBalance.objects.filter(token=self).count()

    def tokensSharingAddress(self):
        return Token.objects.filter(address=self.address).count()

    def sharesAddress(self):
        return self.tokensSharingAddress() > 1

    def priceMissing(self):
        if self.price == None:
            return True
        return self.price <= 0

    def marketCapMissing(self):
        if self.market_cap == None:
            return True
        return self.market_cap <= 0

class PoolToken(Token):
    class Meta:
        pass

    exchange_name = models.CharField(max_length=100)

    def __str__(self):
        q = Token.objects.filter(poolmember__pool_token=self)
        r = '-'.join(map(str, q.all()))
        if r == '':
            return self.address + ' (unknown ' + self.exchange_name + ' asset pool)'
        return self.exchange_name + ' ' + r + ' pool'

    def update(self):
        supply = self.chain.getSupply(self.address)

        supply = Decimal(supply) / Decimal(f'1e{self.decimals}')
        self.supply = supply

        balances = PoolMember.objects.filter(pool_token=self)
        tvl = 0
        for balance in balances:
            tvl = tvl + balance.quantity * balance.member.price
        self.market_cap = tvl
        self.price = tvl / supply

        q = Token.objects.filter(poolmember__pool_token=self)
        if q.filter(address=self.address).count() > 0:
            pass
        else:
            r = '-'.join(map(str, q.all()))
            if r == '':
                self.name = self.address + ' (unknown ' + self.exchange_name + ' asset pool)'
            self.name = self.exchange_name + ' ' + r + ' pool'

        self.save()

    def poolMembers(self):
        return PoolMember.objects.filter(pool_token=self).count()

    def memberCountWrong(self):
        return self.poolMembers() != 2

class PoolMember(models.Model):
    class Meta:
        pass

    member = models.ForeignKey(Token, on_delete=models.CASCADE)
    pool_token = models.ForeignKey(PoolToken, on_delete=models.CASCADE, related_name='members')
    quantity = models.DecimalField(max_digits=65, decimal_places=10, blank=True, null=True)

    def __str__(self):
        return str(self.member)

    def identicalEntryCount(self):
        return PoolMember.objects.filter(member=self.member, pool_token=self.pool_token).count()

    def hasDuplicates(self):
        return self.identicalEntryCount() > 1

    def quantityMissing(self):
        if self.quantity == None:
            return True
        return self.quantity <= 0

class StateMachine(models.Model):
    class Meta:
        pass

    token_offset = models.IntegerField()
    pool_offset = models.IntegerField()
    treasury_offset = models.IntegerField()

    def save(self, *args, **kwargs):
        self.pk = 1
        super(StateMachine, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(
                pk=1,
                defaults={'token_offset': 0, 'pool_offset': 0, 'treasury_offset': 0}
        )
        return obj

class DAO(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    treasury = models.CharField(max_length=100)
    token = models.ForeignKey(Token, on_delete=models.CASCADE)
    website = models.CharField(max_length=100, null=True)
    public = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        path = reverse('dao')
        return f'{path}#{self.pk}'

    def treasuryUrl(self):
        return self.token.chain.getUrl(self.treasury)

    def netCurrentAssetValue(self):
        if hasattr(self, '_netCurrentAssetValue'):
            return self._netCurrentAssetValue

        balances = TreasuryBalance.objects.filter(dao=self)
        ncav = 0
        for balance in balances:
            ncav = ncav + balance.quantity * balance.token.price

        self._netCurrentAssetValue = ncav
        return ncav

    def priceToNetCurrentAssetValue(self):
        ncav = self.netCurrentAssetValue()
        return self.token.market_cap / ncav

class TreasuryManager(models.Manager):
    def getBalancesFor(self, dao):
        return super().get_queryset().filter(dao=dao).annotate(value_locked=models.F('quantity')*models.F('token__price')).order_by('-value_locked')

class TreasuryBalance(models.Model):
    dao = models.ForeignKey(DAO, on_delete=models.CASCADE)
    token = models.ForeignKey(Token, on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=65, decimal_places=10, blank=True, null=True)

    objects = TreasuryManager()

    def __str__(self):
        return str(self.token)

    def identicalEntryCount(self):
        return TreasuryBalance.objects.filter(dao=self.dao, token=self.token).count()

    def hasDuplicates(self):
        return self.identicalEntryCount() > 1

    def quantityMissing(self):
        if self.quantity == None:
            return True
        return self.quantity <= 0
