from colorsys import hsv_to_rgb

from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView

from django.conf import settings

from .models import *

class CachedView():
    def dispatch(self, *args, **kwargs):
        response = super(CachedView, self).dispatch(*args, **kwargs)
        response['Cache-Control'] = settings.CACHE_HEADER
        return response

class HomeView(CachedView, ListView):
    template_name = 'index.html'
    queryset = DAO.objects.filter(public=True)

class DaoView(CachedView, DetailView):
    model = DAO
    template_name = 'dao.html'

    def get_context_data(self, *args, **kwargs):
        result = super(DetailView, self).get_context_data(*args, **kwargs)
        result['balances'] = TreasuryBalance.objects.getBalancesFor(self.object)
        return result

def sendJson(request, pk):
    result = generateChart(pk)
    response = JsonResponse(result)
    response['Cache-Control'] = settings.CACHE_HEADER
    return response

def generateChart(pk):
    dao = DAO.objects.get(name=pk)
    balances = TreasuryBalance.objects.getBalancesFor(dao)
    names = list(map(lambda x: x.token.name, balances))
    values = list(map(lambda x: x.value_locked, balances))
    result = {
            'labels': names,
            'datasets': [{
                'data': values,
                'backgroundColor': generateColors(len(values))
            }]
    }
    return result

def generateColors(count):
    result = []
    for i in range(int(count/2 + 0.5)):
        interpolated = 2 * i / count
        a = hsv_to_rgb(interpolated / 2, 1, 1 - interpolated / 2)
        b = hsv_to_rgb((interpolated + 1) / 2, interpolated * 0.5 + 0.5, 1)
        result.extend([toHexColor(a), toHexColor(b)])
    return result

def toHexColor(input):
    r = int(input[0] * 255)
    g = int(input[1] * 255)
    b = int(input[2] * 255)
    return(f'#{r:02X}{g:02X}{b:02X}')

class TokenCheckView(ListView):
    template_name = 'tokencheck.html'
    model = Token

class PoolCheckView(ListView):
    template_name = 'poolcheck.html'
    model = PoolToken

class PoolMemberCheckView(ListView):
    template_name = 'poolmembercheck.html'
    model = PoolMember

class BalanceCheckView(ListView):
    template_name = 'balancecheck.html'
    model = TreasuryBalance

