from django.urls import path


from . import views

urlpatterns = [
        path('tokencheck', views.TokenCheckView.as_view()),
        path('poolcheck', views.PoolCheckView.as_view()),
        path('poolmembercheck', views.PoolMemberCheckView.as_view()),
        path('balancecheck', views.BalanceCheckView.as_view()),
        path('dao/json/<slug:pk>', views.sendJson),
        path('dao/<slug:pk>', views.DaoView.as_view(), name='dao'),
        path('', views.HomeView.as_view()),
]
