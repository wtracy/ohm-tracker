from decimal import Decimal
import traceback

from django.core.management.base import BaseCommand, CommandError

import requests

from main.models import *

def increment(value, wrap):
        if wrap > 0:
            return (value+1) % wrap
        return 0


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        state = StateMachine.load()
        try:
            self.updatePrice(state.token_offset)
        except Exception as e:
            print(traceback.format_exc())
        try:
            self.updatePoolBalance(state.pool_offset)
        except Exception as e:
            print(traceback.format_exc())
        try:
            self.updateTreasuryBalance(state.treasury_offset)
        except Exception as e:
            print(traceback.format_exc())
        # TODO: state machine management should really happen inside the state machine class
        count = Token.objects.count()
        state.token_offset = increment(state.token_offset, count)
        count = PoolMember.objects.count()
        state.pool_offset = increment(state.pool_offset, count)
        count = TreasuryBalance.objects.count()
        state.treasury_offset = increment(state.treasury_offset, count)
        state.save()


    def updatePoolBalance(self, index):
        balance = PoolMember.objects.order_by('pool_token', 'member')[index]
        print(f'Updating {balance.member} balance in {balance.pool_token} ...')
        chain = balance.pool_token.chain
        pool = balance.pool_token.address
        token = balance.member.address
        decimals = balance.member.decimals

        balance.quantity = self.findBalance(chain, pool, token, decimals)
        balance.save()

    def updateTreasuryBalance(self, index):
        balance = TreasuryBalance.objects.order_by('dao', 'token')[index]
        print(f'Updating {balance.token} balance in {balance.dao} ...')
        chain = balance.dao.token.chain
        treasury = balance.dao.treasury
        token = balance.token.address
        decimals = balance.token.decimals

        balance.quantity = self.findBalance(chain, treasury, token, decimals)
        balance.save()

    def findBalance(self, chain, pool, token, decimals):
        result = chain.getBalance(pool, token)
        quantity = Decimal(result) / Decimal(f'1e{decimals}')
        return quantity

    def updatePrice(self, index):
        token = Token.objects.order_by('address').select_subclasses()[index]
        print(f'Updating price for {token} ...')
        token.update()
