from django.contrib import admin
from .models import *

admin.site.register(Token)
admin.site.register(PoolToken)
admin.site.register(PoolMember)

admin.site.register(DAO)
admin.site.register(TreasuryBalance)

admin.site.register(Chain)
