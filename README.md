Wrapped Ethereum: 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2
Frax:             0x853d955acef822db058eb8505911ed77f175b99e
Weth-Ohm:         0xfffae4a0f4ac251f4705717cd24cadccc9f33e06
Ohm:              0x383518188c0c6d7730d91b2c03a03c837814a899
LUSD:             0x5f98805A4E8be255a32880FDeC7F6728C6568bA0
Ohm-LUSD:         0xfDf12D1F85b5082877A6E070524f50F6c84FAa6b
Olympus treasury: 0x31F8Cc382c9898b273eff4e0b7626a6987C846E8

Scanner APIs:
etherscan.io
https://ethplorer.io/ <- List of tokens for Ethereum address
polygonscan.com
snowtrace.io (also avascan.info?)
solscan.io

Etherscan URLs:
https://api.etherscan.io/api?module=stats&action=tokensupply&contractaddress={address}&apikey=V3EY284WGX8AM7DIYSEDS3HBAMUPZM611C
https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress={token}&address={pool}&apikey=V3EY284WGX8AM7DIYSEDS3HBAMUPZM611C

DAOs:

Priority:
Olympus
Klima DAO
Otterclam
LobisDAO possibly

Eth low-hanging fruit:
Squid DAO
Kepler DAO
Cerberus DAO
TempleDAO

Polygon low-hanging fruit:
Blob DAO
Atlas USV

TBD: Avalanche
TBD: Solana
TBD: Moonriver/Moonbeam

TODO: Add CoinGecko and/or ohmforks links
TODO: Warn about untracked treasury assets?
TODO: Disable caching for localhost
